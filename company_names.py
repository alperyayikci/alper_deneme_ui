#!/usr/bin/python

import json
import operator
import urllib2
import threading
import time
from sets import Set

company_names = {};

class myThread (threading.Thread):
	def __init__(self, threadID, macInfo, delay):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.macInfo = macInfo
		self.delay = delay
	def run(self):
		# print "Starting Thread-" + str(self.threadID) + ""
		# print self.mac + " , " + str(self.delay)
		time.sleep(self.delay)
		global company_names
		macObject = self.macInfo.split(',')
		mac = macObject[0]
		sniffer = macObject[1]
		if(mac == "ff:ff:ff:ff:ff:ff"):
			company_names[sniffer][mac] = "BROADCAST";
			return;
		req = urllib2.Request("http://www.macvendorlookup.com/api/v2/%s" % (mac));
		time.sleep(self.delay);
		opener = urllib2.build_opener();
		f = opener.open(req);
		info_list = f.read();
		if(info_list == ""):
			company_names[sniffer][mac] = "Undefined"
			# print "Exception" + self.mac
			return;
		# print "No exception"
		json_info_list = json.loads(info_list);
		company_names[sniffer][mac] = json_info_list[0]["company"]
		print mac + " : " + company_names[sniffer][mac];
		# print "Exiting Thread-" + str(self.threadID) + ""

def main():
	getCompanyNames(0);

def getCompanyNames(selected_pcap):
	global company_names
	mac_addresses = Set();
	pcap_files = Set();
	threads = []

	with open("link_all.json") as data_file:
		data = json.load(data_file);
	for i in range(0,len(data)):
		mac_addresses.add(data[i]["wlan.da"] + "," + data[i]["sniffer"])
		mac_addresses.add(data[i]["wlan.sa"] + "," + data[i]["sniffer"])
		mac_addresses.add(data[i]["wlan.ta"] + "," + data[i]["sniffer"])
		mac_addresses.add(data[i]["wlan.ra"] + "," + data[i]["sniffer"])
		pcap_files.add(data[i]["sniffer"])

	i = 1;
	for pcap_file in pcap_files:
		company_names[pcap_file] = {};
	for macInfo in mac_addresses:
		thread = myThread(i,macInfo,0);
		i += 1;
		thread.start();
		threads.append(thread)
	for t in threads:
		t.join()
	print company_names
	#print "Exiting Main Thread"
	with open('company_names.json', 'w') as outfile:
		json.dump(company_names, outfile);

if __name__ == "__main__":
	main()