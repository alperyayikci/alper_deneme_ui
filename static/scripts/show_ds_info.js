function showDs(selected_pcap,ds_number){
    var data = {"selectedValue" : selected_pcap, "ds_number" : ds_number};
    var url = "http://" + location.host + "/mac_info_ds";
    var request = $.ajax({
        type: 'GET',
        url: url,
        data: data,
        async: false,
        accepts: JSON,
    });

    request.success(function(data){
        console.log("Ds-" + ds_number + " Success");
        console.log(data);
        result = data.result;
        return true;
    });
    $('#ds_'+ ds_number +'_div').html("<h2> From Ds: "+ ds_number[0] +
                                     " , To Ds : "+ ds_number[1] +" </h2><br>");
    for (var i = 0 ; i < result.length; i++) {
        createCheckbox('ds_' + ds_number + '_div', 'ds_' + ds_number + 
                        '_checkbox', 'ds_' + ds_number + '_' + i,
                        result[i]['wlan.ta'] + "-" + result[i]['wlan.da'],
                        result[i]['wlan.ta'] + " --> "+ result[i]['wlan.da'] +
                        " (" + result[i]['packet_num'] + ")" , 'li2');
    }
}