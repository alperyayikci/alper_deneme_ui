function showTx(selected_pcap){
    var data = {"selectedValue" : selected_pcap};
    var url = "http://" + location.host + "/mac_info_tx";
    var request = $.ajax({
        type: 'GET',
        url: url,
        data: data,
        async: false,
        accepts: JSON,
    });

    request.success(function(data){
        console.log("Tx Success");
        console.log(data);
        result = data.result;
        return true;
    });

    $('#tx_div').html("<h2> Transmitter Mac Addresses: </h2><br>");
    for (var i = 0 ; i < result.length; i++) {
        createCheckbox('tx_div', 'tx_checkbox', 'tx_' + i,
                        result[i][0], result[i][0] + " (" + result[i][1] + ")",
                        'li3');
    }
}