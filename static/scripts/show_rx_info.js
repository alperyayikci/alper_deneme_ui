function showRx(selected_pcap){
    var data = {"selectedValue" : selected_pcap};
    var url = "http://" + location.host + "/mac_info_rx";
    var request = $.ajax({
        type: 'GET',
        url: url,
        data: data,
        async: false,
        accepts: JSON,
    });

    request.success(function(data){
        console.log("Rx Success");
        console.log(data);
        result = data.result;
        return true;
    });
    $('#rx_div').html("<h2> Reciever Mac Addresses: </h2><br>");
    for (var i = 0 ; i < result.length; i++) { 
        createCheckbox('rx_div', 'rx_checkbox', 'rx_' + i,
                        result[i][0], result[i][0] + " (" + result[i][1] + ")", 
                        'li3');
    }
}