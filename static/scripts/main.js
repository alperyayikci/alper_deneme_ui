// Object that holds all of the selected checkboxes when pressed process button
var selectedCheckboxes = {"pcap" : "", "all" : false, "rx" : [], "tx" : [],
                          "linked" : [], "config_fields" : {"extras" : [],
                          "text_fields" : []}, "ds" : {"00" : [] , "01" : [] ,
                          "10" : [] , "11" : [] } };
var companyNames = [];
// Create drop down list of pcap files.
$(function() {
    var data = {};
    var url = "http://" + location.host + "/get_pcap_files";
    var get_pcap_files = $.ajax({
            type: 'GET',
            url: url,
            data: data,
            async: false,
            accepts: JSON,
        });
    get_pcap_files.success(function(data){
            console.log("Get pcap files success");
            console.log(data);
            result = data.result;
            return true;
        });
    //console.log(result);
    var pcap_ddlist = document.getElementById("pcap_list");
    for(var  i = 0; i < result.length ; i++){
        var option = document.createElement('option');
        option.value = i;
        option.text = result[i];
        pcap_ddlist.add(option);
    }

});

// SHOW BUTTON //
$(function() {
    $('#show_data').click(function() {
        // Display and get the selected .pcap file from the drop down list.
        var pcap_ddlist = document.getElementById("pcap_list");
        var selectedOption = pcap_ddlist.options[pcap_ddlist.selectedIndex];
        selectedCheckboxes["pcap"] = selectedOption.text;
        console.log("Selected : " + selectedOption.text);

        var show_div = document.getElementById("show_div");
        show_div.style.display = "block";

        // SHOW ALL //
        $('#all_div').html("<h2> All Mac Addresses: </h2><br>");
        $('#all_div').append("<li><input type='checkbox' name='all_checkbox' " +
                             "id='all_checkbox' value='all'/>" + 
                             "<label><span></span>All</label></li>");

        showRx(selectedOption.value);
        showTx(selectedOption.value);
        showLinked(selectedOption.value);
        //showFields(selectedOption.value);
        showCompany(selectedOption.value);
        showDs(selectedOption.value, "00");
        showDs(selectedOption.value, "01");
        showDs(selectedOption.value, "10");
        showDs(selectedOption.value, "11");

        //Create process button
        $('#process_div').html("<button onclick='processData()'>" +
                                 "Process</button><br/>");

        //Create query button        
        $('#query_div').html("<button onclick='queryData()'>" +
                                "Query </button><br/>");

    });
});

// PROCESS BUTTON //
function processData(){
    var pcap_ddlist = document.getElementById("pcap_list");
    var selectedOption = pcap_ddlist.options[pcap_ddlist.selectedIndex];
    trackCheckboxes(selectedOption.text);

    console.log("All: ");
    console.log(selectedCheckboxes["all"]);
    console.log("Rx: ");
    console.log(selectedCheckboxes["rx"]);
    console.log("Tx: ");
    console.log(selectedCheckboxes["tx"]);
    console.log("Linked: ");
    console.log(selectedCheckboxes["linked"]);
    console.log("Config Fields: ");
    console.log(selectedCheckboxes["config_fields"]);
    console.log("Ds: ");
    console.log(selectedCheckboxes["ds"]);

    // // Writing config_fields to config.ini
    // var extras = jQuery.extend(true, [],
    //                selectedCheckboxes["config_fields"]["extras"]);
    // console.log(extras);
    // var text_fields = jQuery.extend(true, [],
    //                    selectedCheckboxes["config_fields"]["text_fields"]);
    // console.log(text_fields);
    // var data = {"extras" : extras , "text_fields" : text_fields};
    // console.log(data);
    // var url = "http://" + location.host + "/set_fields";
    // var request = $.ajax({
    //     type: 'GET',
    //     url: url,
    //     data: data,
    //     async: false,
    //     accepts: JSON,
    // });

    // request.success(function(data){
    //     console.log("Set Fields Success");
    //     console.log(data);
    //     result = data.result;
    //     return true;
    // });

    // Empty selectedCheckboxes object after all done.
    emptySelectedCheckboxes();
}

// QUERY BUTTON //
function queryData(){
    var pcap_ddlist = document.getElementById("pcap_list");
    var selectedOption = pcap_ddlist.options[pcap_ddlist.selectedIndex];
    trackCheckboxes(selectedOption.text);

    // Send the selected checkboxes data to server for querying database.
    var data = {"data" : JSON.stringify(selectedCheckboxes)};
    console.log(data);
    var url = "http://" + location.host + "/query_data";
    var request = $.ajax({
        type: 'GET',
        url: url,
        data: data,
        async: false,
        accepts: JSON,
    });

    request.success(function(data){
        console.log("Query Success");
        console.log(data);
        result = data.result;
        return true;
    });

    // Empty selectedCheckboxes object after all done.
    emptySelectedCheckboxes();

}

// TRACK CHECKBOX EVENTS// 
function trackCheckboxes(selected_pcap_name){
    // Trackes button checked events and store it on slectedCheckboxes object.
    $('input[type="checkbox"]:checked').each(function () {
        //Track each checked boxes and storing values in selectedCheckboxes.
        if(this.name == "all_checkbox"){

            selectedCheckboxes["all"] = true;

        }else if(this.name == "rx_checkbox" || this.name == "tx_checkbox"){
            var prefix = this.name.substring(0,2);
            var mac = this.value.substring(0,this.value.length);
            selectedCheckboxes[prefix].push(mac);
            
        }else if(this.name == "linked_checkbox" || 
                 this.name.substring(0,2) == "ds"){

            //Parse the mac addresses.
            //(Exp:value="88:41:fc:1b:97:c3-ff:ff:ff:ff:ff:ff" )
            var index_of_ = this.value.indexOf('-');
            var mac_ta = this.value.substring(0,index_of_);
            var mac_da = this.value.substring(index_of_+1,this.value.length);
            
            if(this.name == "linked_checkbox"){

                // Create a linked object similar to the one in link_all.json
                var linked_object = {};
                linked_object["wlan.da"] = mac_da;
                linked_object["sniffer"] = selected_pcap_name
                linked_object["wlan.ta"] = mac_ta;

                selectedCheckboxes["linked"].push(linked_object);

            }else{

                var ds_number = this.name.substring(3,5);
                console.log("ds-" + ds_number + "-Checked : " + this.value);
                // Create a linked object similar to the one in ds_data.json
                var ds_object = {};
                ds_object["tods"] = ds_number[1];
                ds_object["wlan.da"] = mac_da;
                ds_object["fromds"] = ds_number[0];
                ds_object["pcap"] = selected_pcap_name;
                ds_object["wlan.ta"] = mac_ta;
                selectedCheckboxes["ds"][ds_number].push(ds_object);  

            }
        }else if(this.name=="extras_checkbox" ||
                 this.name=="text_fields_checkbox"){

            prefix = this.name.substring(0,this.name.lastIndexOf('_'));
            selectedCheckboxes["config_fields"][prefix].push(this.value);

        }
    });
}

function createCheckbox(div_name, name, id, value, text, style_class){
    $('#'+div_name)
        .append("<li class='" + style_class +  
                "'><input type='checkbox' name='"+ name + "' id='" + id +
                "' value='" + value + "'><label>"+ text + "</label></li>"
        );
}

function emptySelectedCheckboxes(){
    selectedCheckboxes["all"] = false;
    selectedCheckboxes["rx"].length = 0
    selectedCheckboxes["tx"].length = 0;
    selectedCheckboxes["linked"].length = 0;
    selectedCheckboxes["config_fields"]["extras"].length = 0;
    selectedCheckboxes["config_fields"]["text_fields"].length  = 0;
    selectedCheckboxes["ds"]["00"].length = 0;
    selectedCheckboxes["ds"]["01"].length = 0;
    selectedCheckboxes["ds"]["10"].length = 0;
    selectedCheckboxes["ds"]["11"].length = 0;
}