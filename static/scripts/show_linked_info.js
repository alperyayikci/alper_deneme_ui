function showLinked(selected_pcap){
    var data = {"selectedValue" : selected_pcap};
    var url = "http://" + location.host + "/mac_info_linked";
    var request = $.ajax({
        type: 'GET',
        url: url,
        data: data,
        async: false,
        accepts: JSON,
    });

    request.success(function(data){
        console.log("Linked Success");
        console.log(data);
        result = data.result;
        return true;
    });
    $('#linked_div').html("<h2> Linked Mac Addresses: </h2><br>");
    for (var i = 0 ; i < result.length; i++) {

        createCheckbox('linked_div', 'linked_checkbox', 'linked_' + i,
                        result[i]['wlan.ta'] + "-" + result[i]['wlan.da'],
                        result[i]['wlan.ta'] + " --> "+ result[i]['wlan.da'] + 
                         " (" + result[i]['packet_num'] + ")" , 'li2');
    }
}