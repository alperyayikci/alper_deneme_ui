function showFields(selected_pcap){
    var data = {"selectedValue" : selected_pcap};
    var url = "http://" + location.host + "/get_fields";
    var request = $.ajax({
        type: 'GET',
        url: url,
        data: data,
        async: false,
        accepts: JSON,
    });

    request.success(function(data){
        console.log("Fields Success");
        console.log(data);
        result = data.result;
        return true;
    });
    $('#fields_div').html("<h2> Config Extras: </h2><br>");
    for (var i = 0 ; i < result["extras"].length; i++) {

        createCheckbox("fields_div", "extras_checkbox", result["extras"][i],
                     result["extras"][i],result["extras"][i]);

    }
    $('#fields_div').append("<h2>Config Text Fields: </h2><br>");
    for (var i = 0 ; i < result["text_fields"].length; i++) {

        createCheckbox("fields_div", "text_fields_checkbox", "fields_" + i,
                     result["text_fields"][i], result["text_fields"][i]);

    }
}