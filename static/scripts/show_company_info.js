function showCompany(selected_pcap){
    var data = {"selectedValue" : selected_pcap};
    var url = "http://" + location.host + "/mac_info_company";
    var request = $.ajax({
        type: 'GET',
        url: url,
        data: data,
        async: false,
        accepts: JSON,
    });

    request.success(function(data){
        console.log("Company Success");
        console.log(data);
        result = data.result;
        return true;
    });
    // Show the company names of rx and tx mac addresses as tooltips on hover.
    companyNames = jQuery.extend(true, [], result);
    for(i = 0; i < companyNames.length; i++){
        for(j = 0; j < companyNames.length; j++){
            if($('#rx_' + i).val() == companyNames[j][0]){
                $('#rx_' + i).parent().prop("title",companyNames[j][1]);
            }else if($('#tx_' + i).val() == companyNames[j][0]){
                $('#tx_' + i).parent().prop("title",companyNames[j][1]);
            }
        }
    }
    $('#company_div').html("<h2>Company names of the mac addresses:</h2><br>");
    for (var i = 0 ; i < result.length; i++) {
        $('#company_div')
            .append("<li class='li2'> " + 
                     result[i][0] + " --> " + result[i][1] + " </li>");  
    }
}