# -*- coding: utf-8 -*-
"""
jQuery Example
~~~~~~~~~~~~~~
A simple application that shows how Flask and jQuery get along.
:copyright: (c) 2015 by Armin Ronacher.
:license: BSD, see LICENSE for more details.
"""
from flask import Flask, jsonify, render_template, request, send_from_directory
from werkzeug.datastructures import ImmutableMultiDict
from werkzeug import secure_filename
import os, json, operator
from company_names import *
from sets import Set

"""
path = os.getcwd() + '/visualizer'
if path not in sys.path:
	sys.path.insert(0, path)
	"""

app = Flask(__name__, static_url_path='')
basedir = os.path.abspath(os.path.dirname(__file__))
from logging import Formatter, FileHandler
handler = FileHandler(os.path.join(basedir, 'log.txt'), encoding='utf8')
handler.setFormatter(
	Formatter("[%(asctime)s] %(levelname)-8s %(message)s", "%Y-%m-%d %H:%M:%S")
	)

app.logger.addHandler(handler)
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
	return '.' in filename and \
	filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@app.route('/js/<path:path>')
def send_js(path):
	return send_from_directory('js', path)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/get_pcap_files', methods=['GET'])
def getPcapFiles():
	# Open rx_all.json file and read all the pcap files and return.
	with open("rx_all.json") as data_file:
		data = json.load(data_file)
	return jsonify(result = data.keys())

@app.route('/mac_info_rx', methods=['GET'])
def getRxAddresses():
	with open("rx_all.json") as data_file:
		data = json.load(data_file)
	selected_pcap = request.args.get('selectedValue');
	# Sort the data corresponding to selected pcap and send it.
	sorted_data = sorted(data.values()[int(selected_pcap)].items(), key=operator.itemgetter(1), reverse=True)
	return jsonify(result = sorted_data)

@app.route('/mac_info_tx', methods=['GET'])
def getTxAddresses():
	with open("tx_all.json") as data_file:
		data = json.load(data_file)
	selected_pcap = request.args.get('selectedValue');
	sorted_data = sorted(data.values()[int(selected_pcap)].items(), key=operator.itemgetter(1), reverse=True)
	return jsonify(result = sorted_data)

@app.route('/mac_info_linked', methods=['GET'])
def getLinkedAddresses():
	with open("link_all.json") as data_file:
		data = json.load(data_file)
	selected_pcap = request.args.get('selectedValue');
	sorted_data = sorted(data.values()[int(selected_pcap)], key=lambda x: x['packet_num'], reverse=True)
	return jsonify(result = sorted_data)

@app.route('/mac_info_company', methods=['GET'])
def getCompanyName():
	selected_pcap = request.args.get('selectedValue');
	# print "Getting company names...";
	# See company_names.py. 
	# This method reads all company names and writes it in company_names.json object.
	# data = getCompanyNames(selected_pcap);
	# print "Done!";
	with open("company_names.json") as data_file:
		data = json.load(data_file)
	sorted_company_names = sorted(data.items(), key=operator.itemgetter(1))
	return jsonify(result = sorted_company_names)

@app.route('/get_fields', methods=['GET'])
def getFields():
	# This part reads all the fields from config.ini file.
	data = {"extras" : [], "text_fields" : []};
	with open('config.ini','r') as config_file:
		lines = config_file.readlines();
		for i in range(0, len(lines)):
			if(lines[i].startswith("[fields]")):
				# Get extras fields
				i = i + 1;
				nextLine = lines[i];
				# Read the first extras field near extras=...
				field1_extras = nextLine[nextLine.find('=')+1:nextLine.find('\n')];
				if(field1_extras != ""): # If empty dont push it.
					data["extras"].append(field1_extras);
				i = i + 1;
				nextLine = lines[i];
				# Read the remaining extras fields till text_fields
				while(nextLine.startswith("text_fields") == False):
					field = nextLine[7:nextLine.find('\n')];
					data["extras"].append(field);
					nextLine = lines[i+1];
					i = i + 1;
			if(lines[i].startswith("text_fields")):
				nextLine = lines[i];
				# Read the first text_field near text_fields=...
				field1_text_fields = nextLine[nextLine.find('=')+1:nextLine.find('\n')];
				if(field1_text_fields != ""): 
					data["text_fields"].append(field1_text_fields);
				i = i + 1;
				# Read text_fields till the end of file.
				for j in range(i,len(lines)):
					nextLine = lines[j];
					field = nextLine[12:nextLine.find('\n')];
					data["text_fields"].append(field);
				i = len(lines);
				break;

	return jsonify(result = data)

# @app.route('/set_fields', methods=['GET'])
# def setFields():
# 	extras = request.args.getlist("extras[]");
# 	text_fields = request.args.getlist("text_fields[]");
# 	data = {"extras" : extras , "text_fields" : text_fields};
# 	f = open('config.ini','r');
# 	lines = f.readlines();
# 	f.close();
# 	endOfFile = 0;
# 	for i in range(0, len(lines)):
# 		# Dont write anything and take the lines as it is until [fields] line.
# 		if(lines[i].startswith("[fields]")):
# 			# Write extras
# 			i = i + 1;
# 			if(len(extras) == 0):
# 				lines[i] = "extras=\n";
# 				i = i + 1;
# 			else:
# 				# Write first extras field near extras=...
# 				lines[i] = "extras=" + extras[0] + "\n";
# 				i = i + 1;
# 				# Write remaining extras fields.
# 				for j in range(1,len(extras)):
# 					lines[i] = "       " + extras[j] + "\n";
# 					i = i + 1;
# 			# Write text fields
# 			if(len(text_fields) == 0):
# 				lines[i] = "text_fields=\n";
# 				i = i + 1;
# 			else:
# 				# Write first text_field near text_fields=...
# 				lines[i] = "text_fields=" + text_fields[0] + "\n";
# 				i = i + 1;
# 				# Write remaining text_fields.
# 				for j in range(1,len(text_fields)):
# 					lines[i] = "            " + text_fields[j] + "\n";
# 					i = i + 1;
# 			endOfFile = i;
# 	data = lines[0:endOfFile];
# 	# Write over config.ini file new lines.
# 	os.remove('config.ini');
# 	f = open('config.ini','w')
# 	f.writelines(data)
# 	f.close()
# 	return jsonify(result = data)

@app.route('/mac_info_ds', methods=['GET'])
def getDSData():
	with open("ds_data.json") as data_file:
		data = json.load(data_file)
	selected_pcap = request.args.get('selectedValue');
	ds_number = request.args.get('ds_number');
	# Send the data corresponding to selected pcap
	sorted_data = sorted(data.values()[int(selected_pcap)][ds_number],
	              key=lambda x: x['packet_num'], reverse=True)
	return jsonify(result = sorted_data)

@app.route('/query_data', methods=['GET'])
def queryData():
    data = request.args.get('data')
    selectedCheckboxes = json.loads(data)
    print selectedCheckboxes
    if selectedCheckboxes["all"] == True:
        print "SELECT * FROM packets WHERE \"sniffer\"=\"./" + selectedCheckboxes["pcap"] + "\""
    else:
        print generateQueryString(selectedCheckboxes)
    return jsonify(result = selectedCheckboxes)

def generateQueryString(selectedCheckboxes):
    baseStr = "SELECT * FROM packets WHERE \"sniffer\"=\"./" + selectedCheckboxes["pcap"] + "\" AND ("
    resultStr = baseStr
    """RX"""
    rxStr = ""
    if len(selectedCheckboxes["rx"]) == 0:
        resultStr += " "
    elif len(selectedCheckboxes["rx"]) == 1:
        rxStr = "((\"wlan.da\"=\"" + selectedCheckboxes["rx"][0] + "\"))"
    else:
        rxStr = "("
        for i in range(0, len(selectedCheckboxes["rx"])):
            rxStr += "(\"wlan.da\"=\"" + selectedCheckboxes["rx"][i] + "\")"
            if i != len(selectedCheckboxes["rx"]) - 1:
                rxStr += " OR "
        rxStr += ")"
    resultStr += rxStr + " OR "
    """ TX """
    txStr = ""
    if len(selectedCheckboxes["tx"]) == 0:
        resultStr += " "
    elif len(selectedCheckboxes["tx"]) == 1:
        txStr = "((\"wlan.da\"=\"" + selectedCheckboxes["tx"][0] + "\"))"
    else:
        txStr = "("
        for i in range(0, len(selectedCheckboxes["tx"])):
            txStr += "(\"wlan.da\"=\"" + selectedCheckboxes["tx"][i] + "\")"
            if i != len(selectedCheckboxes["tx"]) - 1:
                txStr += " OR "
        txStr += ")"
    resultStr += txStr + " OR "
    """ LINKED """
    linkedStr = ""
    if len(selectedCheckboxes["linked"]) == 0:
        resultStr += " "
    elif len(selectedCheckboxes["linked"]) == 1:
        linkedStr = "((\"wlan.da\"=\"" + selectedCheckboxes["linked"][0]["wlan.da"] + "\") AND (\"wlan.ta\"=\"" + selectedCheckboxes["linked"][0]["wlan.ta"] + "\"))"
    else:
        linkedStr = "("
        for i in range(0, len(selectedCheckboxes["linked"])):
            linkedStr += "((\"wlan.da\"=\"" + selectedCheckboxes["linked"][i]["wlan.da"] + "\") AND (\"wlan.ta\"=\"" + selectedCheckboxes["linked"][i]["wlan.ta"] + "\"))"
            if i != len(selectedCheckboxes["linked"]) - 1:
                linkedStr += " OR "
        linkedStr += ")"
    resultStr += linkedStr + " OR "
    return resultStr

if __name__ == "__main__":
    app.run(
    	host = "127.0.0.1",
		port = int("8005")
    	)

